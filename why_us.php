<?php include ('assets/inc/site/site_mobile_detect.php'); ?>
<!doctype html>
<!--[if lte IE 9]><html class="lteIE9 loading"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="loading"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Welcome to the Solid Accounting Solutions Website. We provide professional accounting and bookeeping services in the greater San Diego, CA area and beyond.">
        <title>Solid Accounting Solutions | Why Us?</title>
        <?php include ('assets/inc/site/site_head_tags.php'); ?>
    </head>
    <body class="why_us <?php echo $deviceType; ?>">
        <?php include ('assets/inc/site/site_header.php'); ?>
        <section class="site">
            <div class="content">
                <div class="page_content">
					<h1>Why You Need Solid Accounting Solutions</h1>
					<p>Solid Accounting Solutions provides simple bookkeeping services for entrepreneurs, individuals, and small businesses who are looking to fulfill everyone’s basic needs: keeping track of what comes in and what goes out. I realize that finances are a private issue, and sensitive to many, but most small business owners generally find themselves with only enough time to focus on operations and “doing” their actual business, and that can often leave the books on the back burner. Some even feel bookkeeping is one of those necessary evils that they can really do without. Some can get by with this approach, until it is time to obtain financing from the bank for a major purchase/loan, or, it is time to visit the tax man with your shoe box of receipts. No matter who your tax preparer is, your tax bill can be significantly reduced if you show up with those receipts already categorized and presented in a Profit & Loss report format.</p>
					<p>People often think of taxes and bookkeeping/accounting as one and the same, and while they are related, they are not synonymous. Maintaining your books regularly allows you to see where your business is at- as it is happening day to day- instead of waiting to see how your taxes turn out, which is basically the big picture after the fact. Bookkeeping gives you current knowledge about the true performance of your business, and that knowledge is power. For example, monthly profit and loss reports can identify which line of sales is bringing the most revenue, and more importantly, the best profit margins. This enables you to decide where you should keep focusing your efforts, or when it’s time to revise your strategy. Having a second pair of eyes on your expenses can give you a real grip on where each dollar is going and if it is really working to its fullest potential. Being able to walk into a bank with an up to date balance sheet clearly identifying your liabilities, assets and equity shows you are proactive. This says you take your finances very seriously, and that the bank can take you seriously as well.</p>
               		<h4>Related Links</h4>
               		<ul>
               		    <li><a href="http://www.proongo.com/blog/7-common-expense-reporting-mistakes-in-quickbooks/" onclick="window.open(this.href); return false;">7 Common Expense Reporting Mistakes in QuickBooks</a></li>
               		</ul>
                </div>
                <?php include ('assets/inc/page/page_sidebar.php'); ?>
            </div>
        </section>
        <?php include ('assets/inc/site/site_footer.php'); ?>
        <?php include ('assets/inc/site/site_scripts.php'); ?>
    </body>
</html>