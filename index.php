<?php include ('assets/inc/site/site_mobile_detect.php'); ?>
<!doctype html>
<!--[if lte IE 9]><html class="lteIE9 loading"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="loading"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Welcome to the Solid Accounting Solutions Website. We provide professional accounting and bookeeping services in the greater San Diego, CA area and beyond.">
        <title>Solid Accounting Solutions | Home</title>
        <?php include ('assets/inc/site/site_head_tags.php'); ?>
    </head>
    <body class="home <?php echo $deviceType; ?>">
        <?php include ('assets/inc/site/site_header.php'); ?>
        <?php include ('assets/inc/home/home_intro.php'); ?>
        <?php include ('assets/inc/home/home_bookkeeping.php'); ?>
        <?php include ('assets/inc/home/home_packages.php'); ?>
        <?php include ('assets/inc/site/site_footer.php'); ?>
        <?php include ('assets/inc/site/site_scripts.php'); ?>
    </body>
</html>