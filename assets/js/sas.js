/*global $, jQuery*/

(function ($) {
    
    "use strict";
    
    /* Window Load */
    
    $(window).load(function () {
        
        // Display IE Message if lower than ie10
            
        if ($('html').hasClass('lteIE9')) {

            $('body').html('<p class="ieMsg">This site is build using HTML5 &amp; CSS3 that is not fully supported in Internet Explorer 9 and below.<br /><a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie">Click Here</a> to upgrade to the latest version of Internet Explorer.</p>');

        }
    
        // Remove 'loading' class after page load
        
        setTimeout(function () {
            
            $('html').removeClass('loading');
            
        }, 250);
        
    });
    
    // Current Page
    
    var pageClass = $('body').attr('class').split(' ')[0];
    
    $('ul.menu li.' + pageClass).addClass('active');
				
	// Toggle Menu

	$('a.menu').click(function () {

		$(this).toggleClass('on');

	});

	// Reset Menu on Resize

	$(window).resize(function () {

		$('a.menu').removeClass('on');

	});

	// Disable CSS Transitions on Resize

	var onResizeEnd;

	function doneResizing() {

        $('body').removeClass('noTransitions');

	}

	$(window).resize(function () {

		clearTimeout(onResizeEnd);

		onResizeEnd = setTimeout(doneResizing, 250);

		$('body').addClass('noTransitions');

	});
    
    // Desktop Parallax Scroll
    
    if ($('body').hasClass('desktop')) {
        
        $(window).bind('load resize scroll', function () {

            var y = $(window).scrollTop();

            $('section.intro div.content').filter(function () {

                return $(this).offset().top < (y + $(window).height()) &&
                       $(this).offset().top + $(this).height() > y;

            }).css('background-position', '100% ' + parseInt(-y / 4) + 'px');

        });
        
    }
    
    // Breadcrumbs
    
    if (!$('body').hasClass('home')) {
        
        var bodyClass = $('body').attr('class').split(' ')[0].replace(/\_/g, ' ').replace(/\b[a-z]/g, function (letter) {
            
            return letter.toUpperCase();
            
        });
        
        var breadCrumbs = '<p class="breadcrumbs"><span>Home</span><span> / </span><span>' + bodyClass + '</span></p>';
        
        $(breadCrumbs).prependTo('div.page_content');
        
    }
	
	/* Testimonials */
	
	var quote = $('aside p.quote');
	
	if ($(quote).length) {
		
		$('<a class="show more">More</a>').insertAfter(quote);
		
		$('a.show').click(function () {
			
			var toggleText = $(this).html() === 'Less' ? 'More' : 'Less';
			
			$(this).toggleClass('more less').html(toggleText).prev(quote).toggleClass('expanded');
			
		});
			
	}

    /* Current Year */

    $('span.cntYear').text((new Date()).getFullYear());
    
}(jQuery));