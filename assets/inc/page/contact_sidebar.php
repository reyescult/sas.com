<aside class="page_sidebar">
	<h4>Accountant / Owner</h4>
	<figure itemscope itemtype="http://schema.org/Person">
		<img src="/assets/img/page/heidi_baxley.jpg" itemprop="image" alt="Heidi Baxley" />
		<figcaption>
			<span itemprop="name">Heidi Baxley</span><br />
			<span itemprop="jobTitle">CTEC # A273189</span>
			<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<span itemprop="addressLocality">San Diego</span>,
				<span itemprop="addressRegion">CA</span>
			</div>
			<span itemprop="telephone"><?php include ('assets/inc/site/site_phone.php'); ?></span>
		</figcaption>
	</figure>
	<h4>Testimonials</h4>
	<?php include ('assets/inc/site/site_testimonials.php'); ?>
</aside>