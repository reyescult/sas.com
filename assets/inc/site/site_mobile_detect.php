<?php
	require_once 'assets/php/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'desktop');
	$scriptVersion = $detect->getScriptVersion();
?>