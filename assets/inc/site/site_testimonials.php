<div class="testimonials">
	<blockquote class="testimonial">
		<p class="quote">Solid Accounting Solutions: the name says it all. They handle all of our Payables and Receivables. This leaves us free to do what we do best.</p>
		<p class="author">Nathon Sweetman, Chief Information Officer<br />Unified Networks</p>
	</blockquote>
	<blockquote class="testimonial">
		<p class="quote">Whereas others get lost in the details, Heidi Baxley finds a way. I appreciate her professionalism and value her service.</p> 
		<p class="author">Nick Nuth, Sales Engineer<br />Allen Power Distribution &amp; Control, Inc.</p>
	</blockquote>
	<blockquote class="testimonial">
		<p class="quote">Solid Accounting Solutions has been a pleasure to work with.  Heidi takes the time to get to know your company and provides recommendations on how best to handle all accounting functions tailored to the specific business.  Heidi also keeps me honest and is a gentle reminder of when I need to take care of something.  Solid Accounting Solutions has become a partner to my business acting as a sounding board and providing streamlined procedures which help me to focus on the business instead of the bookkeeping.</p>
		<p class="author">Matt Ausmus, Managing Member<br />Caffeinated Networks LLC</p>
	</blockquote>
	<blockquote class="testimonial">
		<p class="quote">Heidi is very thorough and patient - I think those are important traits to have in an accountant/bookkeeper. For me, I just couldn't keep up anymore - Heidi made it simple and easy to get started! I thought I would dread having to get everything ready - but it was quite painless! She has worked diligently on getting my accounting and bookkeeping straightened out! I look forward to a long relationship with Heidi. If you are looking for a trustworthy and patient Accountant - Heidi is your girl!!! Thanks for being you Heidi!</p>
		<p class="author">Trudi C. Kayser, Division Leader<br />Five Rings Financial</p>
	</blockquote>
</div>