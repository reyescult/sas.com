<script src="http://use.edgefonts.net/open-sans:n3,n4,n6,n7;league-gothic.js"></script>
<script>
    var cb = function() {
    var l = document.createElement('link'); l.rel = 'stylesheet';
    l.href = '/assets/css/sas.css';
    var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
        webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(cb);
    else window.addEventListener('load', cb);
</script>
<script src="/assets/js/jquery-2.1.1.min.js"></script>
<script src="/assets/js/sas.js"></script>