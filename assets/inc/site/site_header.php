<header class="site">
    <div class="content">
        <a href="/" class="hdr_logo"><img src="/assets/img/site/sas_logo.svg" alt="Solid Accounting Solutions Logo" /></a>
        <nav class="hdr_menu">
			<a class="menu" data-icon="&#xf0c9;">Menu</a>
			<?php include ('assets/inc/site/site_menu.php'); ?>
        </nav>
        <div class="hdr_info">
            <p><span class="tagline">Professional Accounting & Tax Services in San Diego, CA</span><br /><?php include ('assets/inc/site/site_phone.php'); ?></p>
        </div>
    </div>
</header>