<footer class="site">
    <div class="content">
        <div class="ftr_info">
            <div class="copyright">
                <p>Copyright &copy;<span class="cntYear"></span> Solid Accounting Solutions. <span class="reserved">All Rights Reserved.</span></p>
                <p>Mobile-First Website By: <a href="http://www.reyescult.com" class="reyescult" onclick="window.open(this.href); return false;">Reyescult DMD</a></p>
            </div>
        </div>
        <nav class="ftr_menu">
            <?php include ('assets/inc/site/site_menu.php'); ?>
        </nav>
    </div>
</footer>