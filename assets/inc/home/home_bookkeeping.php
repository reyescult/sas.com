<section class="site bookkeeping">
    <div class="content">
        <div class="caption bookkeeping">
            <h2>So, do you really <span>NEED</span> us?</h2>
            <p>Maintaining your books regularly allows you to see where your business is - as it is happening day to day - instead of waiting to see how your taxes turn out. Bookkeeping gives you current knowledge about the true performance of your business, and that knowledge is power.</p>
            <a href="/why_us.php" data-icon="&#xf054;">Learn More</a>
        </div>
    </div>
</section>