<section class="site intro">
    <div class="content">
        <div class="caption intro">
            <h2>Make a <span>SOLID</span> choice!</h2>
            <p>Solid Accounting Solutions provides professional accounting &amp; tax services in the greater San Diego area and beyond.</p>
            <a href="/contact.php" data-icon="&#xf054;">Contact Us</a>
        </div>
    </div>
</section>