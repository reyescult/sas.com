<section class="site packages">
    <div class="content">
        <div class="caption packages">
            <h2>Services/Packages that we offer.</h2>
            <p>The packages below are initial outlines for ongoing service. Custom proposals for trial periods, catch up work, or project work can be provided upon request.</p>
        </div>
        <div class="package basic" data-icon="&#xf042;">
            <h3>Basic</h3>
            <p>Up to 50 transactions, 3 accounts including reconciliations, 1 hour of live communication time and more...</p>
            <a href="/services.php" data-icon="&#xf054;">Learn More</a>
        </div>
        <div class="package standard" data-icon="&#xf1b2;">
            <h3>Standard</h3>
            <p>Up to 125 transactions, 4 accounts including reconciliations, 2 hours of live communication time and more...</p>
            <a href="/services.php" data-icon="&#xf054;">Learn More</a>
        </div>
        <div class="package deluxe" data-icon="&#xf1c0;">
            <h3>Deluxe</h3>
            <p>Up to 200 transactions, 5 accounts including reconciliations, 2 hours of live communication time and more...</p>
            <a href="/services.php" data-icon="&#xf054;">Learn More</a>
        </div>
        <div class="disclaimer packages">
            <p>* These packages are negotiable based on the individual needs of the client.</p>
        </div>
    </div>
</section>