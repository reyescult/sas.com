<?php include ('assets/inc/site/site_mobile_detect.php'); ?>
<!doctype html>
<!--[if lte IE 9]><html class="lteIE9 loading"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="loading"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Welcome to the Solid Accounting Solutions Website. We provide professional accounting and bookeeping services in the greater San Diego, CA area and beyond.">
        <title>Solid Accounting Solutions | Services/Packages</title>
        <?php include ('assets/inc/site/site_head_tags.php'); ?>
    </head>
    <body class="services <?php echo $deviceType; ?>">
        <?php include ('assets/inc/site/site_header.php'); ?>
        <section class="site">
            <div class="content">
                <div class="page_content">
                	<h1>Services/Packages</h1>
					<p>The packages below are initial outlines for ongoing service. Custom proposals for trial periods, catch up work, or project work can be provided upon request.</p>
					<div class="service basic" data-icon="&#xf042;">
					    <h2><span>Basic:</span> $300/mo</h2>
					    <p>Includes: Up to 50 transactions (income and expense), 3 accounts (Checking, CC, Square, etc.) including reconciliations, 1 hour of live communication time (via phone/in person) to review uncategorized or questionable items, financial statements provided on quarterly basis, and at year end. Client responsible for providing expense/deposit detail prior to entry, and for diligent document management (eg scan to Dropbox or provide scan copies via email).</p>
					</div>
					<div class="service standard" data-icon="&#xf1b2;">
					    <h2><span>Standard:</span> $400/mo</h2>
					    <p>Includes: Up to 125 transactions (income and expense), 4 accounts (Checking, CC, Square, etc.) including reconciliations, 2 hours of live communication time (via phone/in person) to review uncategorized or questionable items, financial statements provided on monthly basis and at year end. Expenses/deposits initially processed based on statement data, and then reviewed with client for explanation/detail.</p>
					</div>
					<div class="service deluxe" data-icon="&#xf1c0;">
					    <h2><span>Deluxe:</span> $600/mo</h2>
					    <p>Includes: Up to 200 transactions (income and expense), 5 accounts (Checking, CC, Square, etc.) including reconciliations, 2 hours of live communication time (via phone/in person) to review uncategorized or questionable items, financial statements provided on monthly basis and at year end. Expenses/deposits initially processed based on statement data, and then reviewed with client for explanation/detail. BONUS: Invoicing, A/R review, collections, A/P review, bill pay, and manage customer and vendor disputes/discrepancies.</p>
					</div>
					<div class="service info" data-icon="&#xf0ea;">
					    <h2><span>Custom:</span></h2>
					    <p>These packages are negotiable based on the individual needs of the client. SAS will provide engagement letters prior to accepting payments. Pricing may be adjusted accordingly should engagements require additional time to perform due to delayed communications or missing data. Every effort will be made to provide quality service within the terms of the engagement. </p>
					    <p>All packages assume client grants SAS view-only online access to bank and credit card accounts (where available) to reduce communication time and ensure ease of use in verifying/explaining transactions. At this time, SAS does not offer payroll services, but will provide 1099 services for independent contractors.</p>
					</div>
                </div>
                <?php include ('assets/inc/page/page_sidebar.php'); ?>
            </div>
        </section>
        <?php include ('assets/inc/site/site_footer.php'); ?>
        <?php include ('assets/inc/site/site_scripts.php'); ?>
    </body>
</html>