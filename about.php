<?php include ('assets/inc/site/site_mobile_detect.php'); ?>
<!doctype html>
<!--[if lte IE 9]><html class="lteIE9 loading"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="loading"><!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta name="robots" content="noindex, nofollow" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="Welcome to the Solid Accounting Solutions Website. We provide professional accounting and bookeeping services in the greater San Diego, CA area and beyond.">
        <title>Solid Accounting Solutions | About</title>
        <?php include ('assets/inc/site/site_head_tags.php'); ?>
    </head>
    <body class="about <?php echo $deviceType; ?>">
        <?php include ('assets/inc/site/site_header.php'); ?>
        <section class="site">
            <div class="content">
                <div class="page_content">
                	<h1>About Solid Accounting Solutions</h1>
					<p>Bookkeeping, accounting and taxes are essential to any profitable business, and this is how I selected my profession. My grandmother was an entrepreneur and small business owner, crafting stained glass kaleidoscopes by hand. Growing up around her business, I became aware of the importance of proper money management. This helped to foster a concern for fiscal responsibility in general, for myself and for others.</p>
                    <p>I decided to focus on a career that provided a necessity, not a luxury, and the ability to help other entrepreneurs realize their business goals and dreams as well. I take pride in helping others to help themselves and grow their businesses. I accomplish this by taking care of the basics so they can focus on doing whatever it is they do best.</p>
                    <p>I obtained a Bachelor’s degree in Accounting, but well aware that business cannot be learned in a classroom alone, I spent twelve years working my way through the ranks of the electrical distribution industry. This would include roles in AP, AR, Freight Auditing, Operations, Warehouse, Returns &amp; Discrepancies, Project Billing, Logistics, and Inside Sales. It was a real life business education that taught me how the different functions of a company integrate, why they need each other, and why they need to respect each other. Most importantly, it taught me diplomacy, and the value of building and maintaining relationships.</p>
                    <h4>Client Testimonials</h4>
                    <?php include ('assets/inc/site/site_testimonials.php'); ?>
                </div>
                <?php include ('assets/inc/page/page_sidebar.php'); ?>
            </div>
        </section>
        <?php include ('assets/inc/site/site_footer.php'); ?>
        <?php include ('assets/inc/site/site_scripts.php'); ?>
    </body>
</html>